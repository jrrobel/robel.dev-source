## Welcome to Robel.dev!

Hello! My name is Jonathon Robel, and I'm a junior currently attending the Colorado
School of Mines. My planned major is Computer Science. [More about me >>](https://robel.dev/page/about)

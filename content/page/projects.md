---
title: Projects
subtitle: My computer science-related projects
date: 2021-07-18
comments: false
---

Going to hackathons with the Mines ACM Student Organization has helped show me how much fun it can be (and how easy it is!) to get into new software and technologies.
Below are some of the projects I've worked on; some I've started, some are short projects for hackathons, and some are simple ideas I have
on the backburner and hope to get back around to soon.

## Group Projects

### Mozzarella
Mozzarella is the project behind a few websites that belong to computing clubs at Mines. Websites such as [acm.mines.edu](https://acm.mines.edu)
and [lug.mines.edu](https://lug.mines.edu) run on Mozzarella! It's built on [Turbogears](https://turbogears.org), a web framework based on the Python programming language.  
[Mozzarella's Gitlab Repository >>](https://gitlab.com/ColoradoSchoolOfMines/mozzarella)

### Quotes File Bot
An idea born from the lack of bots available for the [Matrix instant messaging platform](https://matrix.org), Quotesfilebot is a bot written in Go whose purpose is to
keep track of other users' funny quips, memorable sayings, or any other quotes you'd like to remember. The bot listens for users to mention it in reaction to
another user's message, in which case the bot will message the quote to you in a DM. Quotes can also be sent to a personal quotes file, if you have one.  
[Quotes File Bot's Gitlab Repository >>](https://gitlab.com/jrrobel/quotes-file-bot)

### Automated Litter Verification API (CS Field Session)
I took the class CSCI370 (Advanced computer engineering) in the summer of 2021, going into my junior year. I was on a team of 5 students who were tasked with
creating a machine learning model for the [Clean Planet Project](https://cleanplanetproject.org) nonprofit organization. As I had never worked with anything data science
or machine learning related before, it was particularly challenging figuring out how to work with Tensorflow. Specifically, because we were using Google's cloud
platform, Firebase, for app integration, we had to write our cloud functions in Javascript, and so needed to use the Tensorflow.js API.

[comment]: # ([Field Session Retrospective >>](https://robel.dev/page/blog/csci370-retrospective))

## Personal Projects

### Youtube Focus Assist
One thing I found particularly difficult during lockdown was staying focused. Working/studying from home leaves too many opportunities for distractions,
and I found myself constantly going back to one website: Youtube. While simply blocking the website might've worked well, so much of my study material was located on
Youtube, so I still needed to access it. I realized that much of the time I lost on the site was due to the suggestions tab tempting me. This project, a browser extension, attempts
to help reduce distractions on the site, by disabling comments and suggestions when you are using it for studying.  
[youtube-focus-assist's Github Repository >>](https://github.com/jrjonjonjimmyjimjim/youtube-focus-assist)

### NetSweat
As I've worked on account customization for users of the Netsuite ERP platform, it's become increasingly apparent how important it is to keep track of the environment
you're working in. One mistake in a production environment could lead to hours of headache, and potentially require intervention from other administrators on the
platform. Not good. While I can't prevent my own mistakes, I can at least reduce the chance of them happening. This project is a browser extension that warns you if you're
working in a company's production environment, encouraging you to do your work in the sandbox environment instead.  
[net-sweat's Github Repository >>](https://github.com/jrjonjonjimmyjimjim/net-sweat)

## Hackathon Projects

### Stop Motion (AR Enhanced)
Submitted for HackCU VI  
[Devpost page >>](https://devpost.com/software/stop-motion-ar-enhanced)  
[Github Repository >>](https://github.com/jhgarner/StopMotion)

### Drive, Don't Fly
Submitted for HackUTD VI  
[Devpost page >>](https://devpost.com/software/drive-don-t-fly)  
[Gitlab Repository >>](https://gitlab.com/NickTheSecond/hackutd)
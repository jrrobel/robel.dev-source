---
title: About me
subtitle: Junior studying Computer Science at Colorado School of Mines
comments: false
cover-img: "/img/20210815_103936.jpg"
---

Hello! My name is Jonathon Robel, and I'm an undergraduate studying Computer Science at the Colorado School of Mines.
I am also currently working part-time at [Summit IT](https://summitit.com), designing ZPL label layouts and writing
client- and server-side Javascript to customize business logic within the [Netsuite](https://netsuite.com) ERP platform.

I am originally from San Antonio, Texas, and more recently from Pagosa Springs, Colorado.

### Involvement

I am currently the Vice Chair for the [Mines Association of Computing Machinery](https://acm.mines.edu) and the Treasurer for the [Mines
Linux Users Group](https://lug.mines.edu). I also contribute to the [Mozzarella](https://gitlab.com/ColoradoSchoolOfMines/mozzarella) project,
a web application that runs multiple computing clubs' websites at the Colorado School of Mines.

### Hobbies

In my free time, I enjoy playing board games and tinkering around with Linux. Whenever I can get outside, I enjoy hiking and snowboarding.
I also enjoy 4-wheeling around the rocky mountains. Whenever I'm couped up inside, I enjoy playing video games; most often exploration games
or *Need For Speed* games. I'm also an avid *Beat Saber* player.
